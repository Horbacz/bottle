import java.util.ArrayList;

public class BottleBox {
    private ArrayList<Bottle> bottle = new ArrayList<>(10);

    BottleBox() {
        giveBottle();
    }

    private void giveBottle() {
        bottle.add(0, new Bottle(5, "firstBottle"));
        bottle.add(1, new Bottle(10, "secondBottle"));
        bottle.add(2, new Bottle(5, "thirdBottle"));
        bottle.add(3, new Bottle(10, "fourthBottle"));
        bottle.add(4, new Bottle(5, "fifthBottle"));
        bottle.add(5, new Bottle(10, "sixthBottle"));
        bottle.add(6, new Bottle(5, "seventhBottle"));
        bottle.add(7, new Bottle(10, "eighthBottle"));
        bottle.add(8, new Bottle(5, "ninthBottle"));
        bottle.add(9, new Bottle(10, "tenthBottle"));
    }
}
